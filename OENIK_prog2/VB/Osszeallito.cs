﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    class Osszeallito
    {
        Random rand = new Random();
        static Fa<INyelviMunka> bfa = new Fa<INyelviMunka>();
        static List<Konferencia> konf = new List<Konferencia>();
        static INyelviMunka[] tolmacsok;
        static Nyelvek[] nyelvek;
        static int[] megoldasok;
        BackTrack btrack;
        bool modositE = false;


        void MegbetegedesTortent(INyelviMunka aktTolmacs)
        {
            modositE = false;
            for (int i = 0; i < megoldasok.Length; i++)
            {
                if (aktTolmacs == tolmacsok[megoldasok[i]])
                    modositE = true;
            }
            bfa.Torles(aktTolmacs);
        }


        void Megbetegites()
        {
            foreach (INyelviMunka akt in bfa.Bejaras())
            {
                if (akt is Tolmacs)
                {
                    if (rand.Next(11) > 5)
                    {
                        (akt as Tolmacs).Betegseg();
                    }
                }
            }
        }

        public void KezeloFelulet()
        {
            Console.WriteLine("A program tartalmaz már beépített konferenciákat, melyeknek van neve, nyelvigénye (több nyelvet is képes kezelni) és időtartama.\nA program lehetőséget nyújt új konferenciák létrehozására a konzolon, amely segít kielégíteni a kívánt nyelvi igényeket (optimálisan: a legolcsóbb órabéreket figyelembe véve) a beépített Tolmácsok és TolmácsCégek által.");

            bool vege = false; // A program végét jelző változó.

            StreamReader sr = new StreamReader("konferenciak.txt");
            while (!sr.EndOfStream)
            {
                string sor = sr.ReadLine();
                string[] tomb = sor.Split(',');
                string konfnev = tomb[0];
                string konfnyelv = tomb[1];
                string ar = tomb[2];
                konf.Add(new Konferencia(konfnev, konfnyelv, int.Parse(ar)));
            }

            sr = new StreamReader("tolmacsok.txt");
            while (!sr.EndOfStream)
            {
                string sor = sr.ReadLine();
                string[] tomb = sor.Split(',');
                string tolmnev = tomb[0];
                string tolmnyelv = tomb[1];
                string ar = tomb[2];
                bfa.HozzaAd(new Tolmacs(tolmnev, tolmnyelv, int.Parse(ar)));
                new Tolmacs(tolmnev, tolmnyelv, int.Parse(ar)).Jelentkezes(konf);
            }

            sr = new StreamReader("tolmacscegek.txt");
            while (!sr.EndOfStream)
            {
                string sor = sr.ReadLine();
                string[] tomb = sor.Split(',');
                string cegnev = tomb[0];
                string ar = tomb[1];
                string engedmeny = tomb[2];
                bfa.HozzaAd(new RandomAkciosTolmacsCeg(cegnev, int.Parse(ar), int.Parse(engedmeny)));
            }
            // Konferenciák létrehozása.

            tolmacsok = bfa.Bejaras().ToArray();

            do
            {
                Console.WriteLine("\n\n\n\n----- NAVIGÁCIÓ -----------------\n");
                Console.WriteLine("0 - Feladatismertető újra megjelenítése\n1 - Új Konferencia létrehozása\n2 - Konferenciákra tolmácsok jelentkezésének megtekintése\n3 - Optimális beosztás megtekintése\n4 - Megbetegedések befolyásolta beosztás\nexit - Kilépés"); // exitet
                Console.Write("\nVálasztandó opció + Enter: ");
                string s = Console.ReadLine();

                switch (s)
                {
                    case "exit": // Kilépés
                        Environment.Exit(0);
                        break;
                    case "0": // Feladatismertető újra megjelenítése
                        Console.Clear();
                        Console.WriteLine("----- KONFERENCIAKEZELŐ ---------\n");
                        Console.WriteLine("A program tartalmaz már beépített konferenciákat, melyeknek van neve, nyelvigénye (több nyelvet is képes kezelni) és időtartama.\nA program lehetőséget nyújt új konferenciák létrehozására a konzolon, amely segít kielégíteni a kívánt nyelvi igényeket (optimálisan: a legolcsóbb órabéreket figyelembe véve) a beépített Tolmácsok és TolmácsCégek által.");
                        break;
                    case "1": // Új Konferencia létrehozása
                        bool kilephet = false;
                        while (!kilephet)
                        {
                            Console.WriteLine("\nÚj Konferencia létrehozása\n");
                            string snev;
                            Console.WriteLine("Konferencia neve: "); snev = Console.ReadLine();
                            string snyelv;
                            Console.WriteLine("Konferencia nyelve(i - szóközzel elválasztva): "); snyelv = Console.ReadLine();
                            int sido;
                            Console.WriteLine("Konferencia időtartama: "); sido = int.Parse(Console.ReadLine());

                            konf.Add(new Konferencia(snev, snyelv, sido));
                            nyelvek = NyelvFeltolt(konf);
                            konf.Sort();

                            Console.WriteLine("Akarsz még új konferenciát létrehozni? igen/nem\n"); string igennem = Console.ReadLine();
                            if (igennem.Equals("igen"))
                            {
                                kilephet = false;
                            }
                            else if (igennem.Equals("nem"))
                            {
                                kilephet = true;
                                Console.Clear();
                            }
                        }
                        break;
                    case "2": // Konferenciákra tolmácsok beosztásának megtekintése
                        Console.Clear();
                        Console.WriteLine("-- LEHETSÉGES KONFERENCIÁK és JELENTKEZŐIK --");
                        for (int i = 0; i < konf.Count; i++)
                        {
                            string seged = "";
                            for (int j = 0; j < konf[i].Nyelviigeny.Length; j++)
                            {
                                seged += konf[i].Nyelviigeny[j] + ", ";
                            }
                            Console.WriteLine("\nA \"{0}\" nevű konferenciára jelentkezők\nNyelvi igény(ek): {1} \nIdőtartam: {2} óra\n"
                                , konf[i].Konfnev, seged.TrimEnd(',', ' '), konf[i].Idotartam);
                            foreach (INyelviMunka akt in bfa.Bejaras())
                            {
                                if (akt is TolmacsCeg && ((akt as TolmacsCeg).Jelentkezes(konf)).Contains(konf[i]))
                                {
                                    Console.WriteLine("\t- A {0} nevű cég jelentkezett az összes nyelvre.", (akt as TolmacsCeg).Nev);
                                }
                                if (akt is Tolmacs && ((akt as Tolmacs).Jelentkezes(konf)).Contains(konf[i]) && konf[i].Nyelviigeny.Contains((akt as Tolmacs).Nyelv))
                                {
                                    Console.WriteLine("\t- {0} jelentkezett a/az {1} nyelvvel.", (akt as Tolmacs).Nev, (akt as Tolmacs).Nyelv);
                                }
                            }
                        }
                        break;
                    case "3": // Optimális beosztás megtekintése.
                        nyelvek = new Nyelvek[NyelvFeltolt(konf).Length];
                        for (int i = 0; i < nyelvek.Length; i++)
                        {
                            nyelvek[i] = NyelvFeltolt(konf)[i];
                        }

                        INyelviMunka segedt;
                        for (int i = 0; i < tolmacsok.Length - 1; i++)
                        {
                            for (int j = (i + 1); j < tolmacsok.Length; j++)
                            {
                                if (tolmacsok[i].Ar(1) > tolmacsok[j].Ar(1))
                                {
                                    segedt = tolmacsok[i];
                                    tolmacsok[i] = tolmacsok[j];
                                    tolmacsok[j] = segedt;
                                }
                            }
                        }

                        btrack = new BackTrack(ref nyelvek, ref tolmacsok);
                        Console.Clear();
                        btrack.Elhelyezes(ref megoldasok);

                        for (int i = 0; i < konf.Count; i++)
                        {
                            Console.WriteLine("A(z) " + konf[i].Konfnev + " konferencia beosztása :\n");
                            for (int j = 0; j < megoldasok.Length; j++)
                            {
                                if (nyelvek[j].KonferenciaIdx == i)
                                {
                                    Console.WriteLine("\t- {0} nyelvre {1} lett beosztva.\n", nyelvek[j].Nyelv, tolmacsok[megoldasok[j]].Nev);
                                }
                            }
                        }
                        break;
                    case "4": // Megbetegedések befolyásolta beosztás
                        Megbetegites();
                        if (modositE)
                        {
                            Console.WriteLine("Sajnos a munkások beosztását módosította a betegségek terjedése.\n\n -- BETEGSÉGEK UTÁNI BEOSZTÁS --");
                            for (int i = 0; i < konf.Count; i++)
                            {
                                Console.WriteLine("A(z) " + konf[i].Konfnev + " konferencia beosztása :\n");
                                for (int j = 0; j < megoldasok.Length; j++)
                                {
                                    if (nyelvek[j].KonferenciaIdx == i)
                                    {
                                        Console.WriteLine("\t- {0} nyelvre {1} lett beosztva.\n", nyelvek[j].Nyelv, tolmacsok[megoldasok[j]].Nev);
                                    }
                                }
                            }
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("\n\nA megbetegedések nem módosították a beosztást.");
                        }
                        break;
                    default:
                        Console.WriteLine("Hibás parancs!");
                        break;
                }
            } while (!vege);
        }

        public Nyelvek[] NyelvFeltolt(List<Konferencia> konf)
        {
            // mennyi nyelvi igény van összesen?
            int sum = 0;
            for (int i = 0; i < konf.Count; i++)
            {
                sum += konf[i].Nyelviigeny.Length;
            }

            Nyelvek[] nyelvIgenyek = new Nyelvek[sum];
            int k = 0;
            for (int i = 0; i < konf.Count; i++)
            {
                for (int j = 0; j < konf[i].Nyelviigeny.Length; j++)
                {
                    nyelvIgenyek[k] = new Nyelvek(konf[i].Nyelviigeny[j], konf[i].Konfnev, i, konf[i].Idotartam);
                    k++;
                }
            }
            return nyelvIgenyek;
        }
    }
}
