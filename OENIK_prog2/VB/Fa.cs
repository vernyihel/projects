﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    class Fa<T> where T : INyelviMunka
    {

        class FaElem
        {
            public T tartalom;
            public FaElem jobb;
            public FaElem bal;
        }
        FaElem gyoker;

        public void HozzaAd(T ujlevel)
        {
            PrivHozzaAd(ref gyoker, ujlevel);
        }
        void PrivHozzaAd(ref FaElem p, T tart)
        {
            if (p == null)
            {
                p = new FaElem();
                p.tartalom = tart;
            }
            else
            {
                int c = p.tartalom.Nev.CompareTo(tart.Nev);
                if (c > 0)
                {
                    PrivHozzaAd(ref p.bal, tart);
                }
                else if (c < 0)
                {
                    PrivHozzaAd(ref p.jobb, tart);
                }
            }
        }

        public bool Torles(T tart)
        {
            return PrivTorles(ref gyoker, tart);
        }
        bool PrivTorles(ref FaElem p, T tart)
        {
            if (p == null)
            {
                return false;
            }
            int c = p.tartalom.Nev.CompareTo(tart.Nev);
            if (c > 0)
            {
                return PrivTorles(ref p.bal, tart);
            }
            else if (c < 0)
            {
                return PrivTorles(ref p.jobb, tart);
            }
            else
            {
                if (p.bal == null)
                {
                    p = p.jobb;
                }
                else if (p.jobb == null)
                {
                    p = p.bal;
                }
                else
                {
                    TorlesKetLevel(p, ref p.bal);
                }
                return true;
            }
        }
        void TorlesKetLevel(FaElem p, ref FaElem r)
        {
            if (r.jobb != null)
            {
                TorlesKetLevel(p, ref r.jobb);
            }
            else
            {
                p.tartalom = r.tartalom;
                r = r.bal;
            }
        }

        public IEnumerable<T> Bejaras()
        {
            Queue<FaElem> V = new Queue<FaElem>();
            V.Enqueue(gyoker);
            while (V.Count > 0)
            {
                FaElem p = V.Dequeue();
                yield return p.tartalom;
                if (p.bal != null)
                {
                    V.Enqueue(p.bal);
                }
                if (p.jobb != null)
                {
                    V.Enqueue(p.jobb);
                }
            }
        }
    }
}