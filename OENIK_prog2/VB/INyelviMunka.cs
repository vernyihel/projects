﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{

    public interface INyelviMunka
    {
        string Nev { get; } // Munka(pl. Konferencia) neve.
        bool Nyelvismeret(string nyelv); // Adott nyelven a nyelvi munka elvégezhető-e.
        int Ar(int ido); // Megadott ideig mennyibe kerül a szolgáltatás.
    }
}
