﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{

    public class Konferencia: IComparable
    {
        string konfnev; // Konferecnia neve.
        string[] nyelviigeny; // Konferencia nyelvi igénye.
        int idotartam; // Konferencia időtartama.

        public string Konfnev
        {
            get { return konfnev; }
        }
        public string[] Nyelviigeny
        {
            get { return nyelviigeny; }
        }
        public int Idotartam
        {
            get { return idotartam; }
        }

        public Konferencia(string Konfnev, string Nyelviigeny, int Idotartam)
        {
            konfnev = Konfnev;
            nyelviigeny = Nyelviigeny.Split(' ');
            idotartam = Idotartam;
        }

        public int CompareTo(object obj)
        {
            return ((obj as Konferencia).Idotartam * (obj as Konferencia).Nyelviigeny.Length).CompareTo(this.idotartam * this.nyelviigeny.Length);
        }

    }
}