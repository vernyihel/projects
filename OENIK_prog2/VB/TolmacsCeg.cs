﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    public abstract class TolmacsCeg : INyelviMunka
    {

        /* A TolmacsCeg osztály csupán egy mintája a tolmácscégeknek.h
           Ennek megfelelően vannak létrehozva egyes tulajdonságai.*/

        string nev; // TolmácsCég neve.
        public string Nev
        {
            get { return nev; }
        }
        public abstract int Ar(int ido);
        public bool Nyelvismeret(string nyelv)
        {
            return true;
        }

        public TolmacsCeg(string Nev)
        {
            nev = Nev;
        }

        public List<Konferencia> Jelentkezes(List<Konferencia> list)
        {
            List<Konferencia> mehet = new List<Konferencia>();
            for (int i = 0; i < list.Count; i++)
            {
                int j = 0;
                while (j > list[i].Nyelviigeny.Length && !Nyelvismeret(list[i].Nyelviigeny[j]))
                {
                    j++;
                }
                if (j < list[i].Nyelviigeny.Length)
                {
                    mehet.Add(list[i]);
                }
            }
            return mehet;
        }
    }
}
