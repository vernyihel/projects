﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    public class BackTrack
    {
        //Fa<INyelviMunka> bfa;
        //List<Konferencia> konferenciak;
        Nyelvek[] nyelvek;
        INyelviMunka[] tolmacsok;

        public BackTrack(ref Nyelvek[] ujNyelvek, ref INyelviMunka[] ujTolmacsok)
        {
            nyelvek = ujNyelvek;
            tolmacsok = ujTolmacsok;

        }

        bool Ft(int szint, int hely)
        {   //szintedik nyelvre az i-edik tolmácsot
            return tolmacsok[hely].Nyelvismeret(nyelvek[szint].Nyelv);
        }

        bool Fk(int hely, int khely)
        {   //hely=akttolmacs;khely=eddigitolmacsok
            return (khely != hely);
        }

        int Josag(int[] megold)
        {
            int sum = 0;
            for (int i = 0; i < nyelvek.Length; i++) sum += tolmacsok[megold[i]].Ar(nyelvek[i].Ido);
            return sum;
        }
        void Backtrack(int szint, ref bool VAN, ref int[] helyez, ref int[] OPT)
        {
            int i = 0;
            while (i < tolmacsok.Length)
            {
                if (Ft(szint, i))
                {
                    int k = 0;
                    while (k < szint && Fk(i, helyez[k]))
                    {
                        k++;
                    }
                    if (k == szint)
                    {
                        helyez[szint] = i;
                        if (szint == nyelvek.Length - 1)
                        {
                            if (!VAN || Josag(helyez) < Josag(OPT))
                            {
                                for (int p = 0; p < helyez.Length; p++)
                                {
                                    OPT[p] = helyez[p];
                                }
                            }
                            VAN = true;
                        }
                        else
                        {
                            Backtrack(szint + 1, ref VAN, ref helyez, ref OPT);
                        }
                    }
                }
                i++;
            }
        }
        public void Elhelyezes(ref int[] megoldasok)
        {
            bool van = false;
            int[] lehet = new int[nyelvek.Length];
            int[] OPT = new int[nyelvek.Length];
            Backtrack(0, ref van, ref lehet, ref OPT);
            megoldasok = OPT;
        }
    }
}