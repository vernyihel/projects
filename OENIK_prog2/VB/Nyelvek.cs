﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    public class Nyelvek
    {
        string nyelv;
        string konferenciaNev;
        int konferenciaIdx;
        int ido;
        public string Nyelv
        {
            get { return nyelv; }
        }
        public string KonferenciaNev
        {
            get { return konferenciaNev; }
        }
        public int KonferenciaIdx
        {
            get { return konferenciaIdx; }
        }
        public int Ido
        {
            get { return ido; }
        }
        public Nyelvek(string ujNyelv, string ujKonferenciaNev, int ujKonferenciaIdx, int ujIdo)
        {
            nyelv = ujNyelv;
            konferenciaNev = ujKonferenciaNev;
            konferenciaIdx = ujKonferenciaIdx;
            ido = ujIdo;
        }
    }
}
