﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    public class RandomAkciosTolmacsCeg : TolmacsCeg
    {

        int ar;
        static Random rand = new Random();

        public override int Ar(int ido) // Az akciós ár beállítása.
        {
            return this.ar * ido;
        }

        public RandomAkciosTolmacsCeg(string AkcioNev, int Ar, int AkcioEngedmeny)
            : base(AkcioNev)
        {
            if (rand.Next(0,10)==1 && Ar>AkcioEngedmeny)
            {
               ar = Ar-AkcioEngedmeny;
            }
            else
            {
                ar = Ar;
            }
        }
    }
}
