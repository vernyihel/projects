﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VB
{
    public delegate void Megbetegedett(INyelviMunka lebetegedett);
    public class Tolmacs : INyelviMunka
    {

        string nev; // Tolmács neve.
        string nyelv; // Tolmács által tudott nyelv.
        int ar; // Tolmács órabére.
        public event Megbetegedett Beteg; // Az esetleges megbetegedést kezelő esemény.
        public string Nev
        {
            get { return nev; }
        }
        public string Nyelv
        {
            get { return nyelv; }
        }
        public int Ar(int ido) // Megadott ideig mennyibe kerül a szolgáltatás.
        {
            return this.ar * ido;
        }
        public bool Nyelvismeret(string nyelv) // Adott nyelven a nyelvi munka elvégezhető-e?!
        {
            List<string> munkanyelvek = nyelv.Split(' ').ToList();
            return (munkanyelvek.Contains(this.nyelv));
        }

        public Tolmacs(string Nev, string Nyelv, int Ar)
        {
            /* Tolmács nevének, nyelvének és órabérének beállítása. */
            nev = Nev.ToLower();
            nyelv = Nyelv.ToLower();
            ar = Ar;
        }

        public void Betegseg()
        {
            /* Megbetegítés metódusa. */
            if (Beteg != null)
            { Beteg(this); }
        }

        public List<Konferencia> Jelentkezes(List<Konferencia> list)
        {
            /* Bekérünk az összes Konferenciáról egy listát, amelyről végül eldöntjük, hogy
               az aktuális Tolmács alkalmas-e az adott Konferenciára. Ha igen, ezt eltároljuk
               egy Konferencia listában, amely később visszakereshető. */
            List<Konferencia> mehet = new List<Konferencia>();

            for (int i = 0; i < list.Count; i++)
            {
                int j = 0;
                while (j > list[i].Nyelviigeny.Length && !Nyelvismeret(list[i].Nyelviigeny[j]))
                {
                    j++;
                }
                if (j < list[i].Nyelviigeny.Length)
                {
                    mehet.Add(list[i]);
                }
            }

            return mehet;
        }
    }
}
