﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog1bead
{
    class Madar
    {
        bool[,] territorium;//NxN
        public Madar(int teruletmeret, int x, int y, int r)
        {
            Territorium = new bool[teruletmeret, teruletmeret];
            //csebisev távolság https://en.wikipedia.org/wiki/Chebyshev_distance
            int aX = Math.Max(0, x - r);
            int fX = Math.Min(Territorium.GetLength(1) - 1, x + r);
            int aY = Math.Max(0, y - r);
            int fY = Math.Min(Territorium.GetLength(0) - 1, y + r);
            for (int i = aX; i <= fX; i++)
            {
                for (int j = aY; j <= fY; j++)
                {
                    Territorium[i, j] = true;
                }
            }
        }

        public bool[,] Territorium
        {
            get
            {
                return territorium;
            }

            set
            {
                territorium = value;
            }
        }

        public string megjelenit()
        {
            string s = "";
            for (int i = 0; i < Territorium.GetLength(0); i++)
            {
                for (int j = 0; j < Territorium.GetLength(1); j++)
                {
                    s += String.Format("{0,6}", Territorium[i, j]);//azonos szélesség
                }
                s += "\n";
            }
            return s;
        }
    }
}
