﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace prog1bead
{
    class Program
    {
        static int[,] segedtomb(Madar[] madarak, int[,] verekedomadarak, int teruletmeret)
        {
            //a verekedő madarak száma az adott helyen
            int db = 0;
            for (int i = 0; i < teruletmeret; i++)
            {
                for (int j = 0; j < teruletmeret; j++)
                {
                    for (int k = 0; k < madarak.Length; k++)
                    {
                        if (madarak[k].Territorium[i, j] == true) db++;
                    }
                    verekedomadarak[i, j] = db;
                    db = 0;
                }
            }
            return verekedomadarak;
        }
        static string A(int x, int y, int r, int[,] verekedomadarak, int adottmadar)
        {
            //A. a senki mással nem verekedő madarakat
            string s = "";
            bool osszes = true;
            int aX = Math.Max(0, x - r);
            int fX = Math.Min(verekedomadarak.GetLength(1) - 1, x + r);
            int aY = Math.Max(0, y - r);
            int fY = Math.Min(verekedomadarak.GetLength(0) - 1, y + r);
            for (int i = aX; i <= fX; i++)
            {
                for (int j = aY; j <= fY; j++)
                {
                    if (verekedomadarak[i, j] != 1)
                    {
                        osszes = false;
                    }
                }
            }
            if (osszes == true) s += (adottmadar + 1) + " ";
            return s;
        }
        static string B(int[,] verekedomadarak)
        {
            //B. azt a legveszélyesebb helyet, ahol a legtöbb verekedés lehet és ezen a helyen a verekedésben résztvevő madarak számát;
            string s = "";
            int maxdb = -1;
            int maxX = -1;
            int maxY = -1;
            for (int i = 0; i < verekedomadarak.GetLength(0); i++)
            {
                for (int j = 0; j < verekedomadarak.GetLength(1); j++)
                {
                    if (verekedomadarak[i, j] > maxdb)
                    {
                        maxdb = verekedomadarak[i, j];
                        maxX = i;
                        maxY = j;
                    }
                }
            }
            s += maxdb + " " + (maxX + 1) + " " + (maxY + 1);
            return s;
        }
        static int C(Madar[] madarak, int teruletmeret)
        {
            //C. azon helyek számát, amely nem tartozik egyetlen madár territóriumához sem!
            int db = 0;
            for (int i = 0; i < teruletmeret; i++)
            {
                for (int j = 0; j < teruletmeret; j++)
                {
                    int k = 0;
                    while (k < madarak.Length && madarak[k].Territorium[i, j] == false)//eldöntés: adott x,y helyen minden madár false-e
                    {
                        k++;
                    }
                    if (k >= madarak.Length) db++;
                }
            }
            return db;
        }
        static int Cv2(int[,] verekedomadarak)
        {
            //C. azon helyek számát, amely nem tartozik egyetlen madár territóriumához sem!
            int db = 0;
            for (int i = 0; i < verekedomadarak.GetLength(0); i++)
            {
                for (int j = 0; j < verekedomadarak.GetLength(1); j++)
                {
                    if (verekedomadarak[i, j] == 0)
                    {
                        db++;
                    }
                }
            }
            return db;
        }
        static void FajlKi(string s)
        {
            File.WriteAllText("MADARAK.KI", s);
        }
        static void Main(string[] args)
        {
            if (File.Exists("MADARAK.KI")) File.Delete("MADARAK.KI");
            if (File.Exists("MADARAK.BE"))
            {
                string s = File.ReadAllText("MADARAK.BE");
                string[] sorok = s.Split(new char[] { ' ', '\n', '\r' });

                int madarakszama = int.Parse(sorok[0]);//M
                int teruletmeret = int.Parse(sorok[1]);//N

                int[] Xt = new int[madarakszama];//x tengely (sor) koordináta
                int[] Yt = new int[madarakszama];//y tengely (oszlop) koordináta
                int[] Tm = new int[madarakszama];//territórium méret (sugár)

                //segédtömbökbe beírás
                int seged = 0;
                for (int i = 3; i < sorok.Length; i += 4)
                {
                    Xt[seged] = int.Parse(sorok[i]);
                    seged++;
                }
                seged = 0;
                for (int i = 4; i < sorok.Length; i += 4)
                {
                    Yt[seged] = int.Parse(sorok[i]);
                    seged++;
                }
                seged = 0;
                for (int i = 5; i < sorok.Length; i += 4)
                {
                    Tm[seged] = int.Parse(sorok[i]);
                    seged++;
                }

                //madarak objektumtömb, értékek beírása
                Madar[] madarak = new Madar[madarakszama];
                for (int i = 0; i < madarak.Length; i++)
                {
                    madarak[i] = new Madar(teruletmeret, Xt[i] - 1, Yt[i] - 1, Tm[i]);
                }

                //ellenőrzés
                Console.WriteLine("Madarak objektumtömb:");
                for (int i = 0; i < madarak.Length; i++)
                {
                    Console.WriteLine(madarak[i].megjelenit());
                }

                //feladatmegoldások

                string fajlki = "";
                int[,] verekedomadarak = new int[teruletmeret, teruletmeret];
                segedtomb(madarak, verekedomadarak, teruletmeret);
                Console.WriteLine("Verekedömadarak:");
                for (int i = 0; i < verekedomadarak.GetLength(0); i++)
                {
                    for (int j = 0; j < verekedomadarak.GetLength(1); j++)
                    {
                        Console.Write(verekedomadarak[i, j] + " ");
                    }
                    Console.WriteLine();
                }

                //A
                Console.Write("\nA: ");
                for (int i = 0; i < madarakszama; i++)
                {
                    Console.Write(A(Xt[i] - 1, Yt[i] - 1, Tm[i], verekedomadarak, i));
                    fajlki += A(Xt[i] - 1, Yt[i] - 1, Tm[i], verekedomadarak, i);
                }

                //B
                Console.WriteLine("\nB: " + B(verekedomadarak));
                fajlki += "\n" + B(verekedomadarak);

                //C
                Console.WriteLine("C: " + C(madarak, teruletmeret));
                fajlki += "\n" + C(madarak, teruletmeret);

                //madarak.ki output
                FajlKi(fajlki);
            }
            else
            {
                Console.WriteLine("Hiba: Nem található vagy nem olvasható a MADARAK.BE fájl.");
            }
            Console.ReadKey();
        }
    }
}
