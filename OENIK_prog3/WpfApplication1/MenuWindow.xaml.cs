﻿// <copyright file="MenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// menü ablaka
    /// </summary>
    public partial class MenuWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// default ctor
        /// </summary>
        public MenuWindow()
        {
            this.InitializeComponent();
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Tutorial_Click(object sender, RoutedEventArgs e)
        {
            TutorialWindow window = new TutorialWindow();
            if (window.ShowDialog() == true)
            {
            }
        }
    }
}
