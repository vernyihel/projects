﻿// <copyright file="Game.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System;
    using System.Linq;
    using System.Timers;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;

    /// <summary>
    /// játék logikája
    /// </summary>
    public class Game : FrameworkElement
    {
        /// <summary>
        /// pálya szélessége
        /// </summary>
        public const int WIDTH = 50;

        /// <summary>
        /// pálya magassága
        /// </summary>
        public const int HEIGHT = 50;

        /// <summary>
        /// powerup módosítók dbszáma
        /// </summary>
        public const int LOOTS = 10;

        /// <summary>
        /// méret
        /// </summary>
        public const int SIZE = 15;

        /// <summary>
        /// időzítő
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// időzítő
        /// </summary>
        private Timer stopper;

        /// <summary>
        /// kígyó (játékos)
        /// </summary>
        private Snake snake;

        /// <summary>
        /// felvehető módosító
        /// </summary>
        private PowerUp[] powerUps;

        /// <summary>
        /// játék elindult-e
        /// </summary>
        private bool isStarted = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// ctor: alapbeállítások meghívása
        /// </summary>
        public Game()
        {
            this.SetDefault();
            this.Loaded += this.Game_Loaded;
        }

        /// <summary>
        /// Gets or sets
        /// sebesség propertyje
        /// </summary>
        public static int Speed { get; set; }

        /// <summary>
        /// Gets or sets
        /// eltelt idő propertyje
        /// </summary>
        public static int TotalTime { get; set; }

        /// <summary>
        /// gomb lenyomás irányításhoz
        /// </summary>
        /// <param name="k">Key k</param>
        public void Game_KeyDown(Key k)
        {
            switch (k)
            {
                case Key.Up:
                    this.snake.NextDirection = 1;
                    break;
                case Key.Right:
                    this.snake.NextDirection = 2;
                    break;
                case Key.Down:
                    this.snake.NextDirection = 3;
                    break;
                case Key.Left:
                    this.snake.NextDirection = 4;
                    break;
            }
        }

        /// <summary>
        /// kirajzolás felüldefiniálása, pálya, kígyó, módosítók megjelenítése
        /// </summary>
        /// <param name="drawingContext">DrawingContext drawingContext</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            for (int i = 0; i < HEIGHT; i++)
            {
                for (int j = 0; j < WIDTH; j++)
                {
                    drawingContext.DrawRectangle(Brushes.White, new Pen(Brushes.White, 1), new Rect(j * SIZE, i * SIZE, SIZE, SIZE));
                }
            }

            for (int i = 0; i < this.snake.Parts.Count; i++)
            {
                drawingContext.DrawRectangle(Brushes.LightGreen, null, new Rect(this.snake.Parts[i].X * SIZE, this.snake.Parts[i].Y * SIZE, SIZE, SIZE));
            }

            for (int i = 0; i < this.powerUps.Length; i++)
            {
                drawingContext.DrawRectangle(this.powerUps[i].CurrentColor, null, new Rect(this.powerUps[i].Position.X * SIZE, this.powerUps[i].Position.Y * SIZE, SIZE, SIZE));
            }
        }

        /// <summary>
        /// játék betöltésénél időzítő
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">RoutedEventArgs e</param>
        private void Game_Loaded(object sender, RoutedEventArgs e)
        {
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(1000);
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();

            this.stopper = new Timer(1000);
            this.stopper.Elapsed += this.Stopper_Elapsed;
            this.stopper.Enabled = true;
        }

        /// <summary>
        /// stopper eltelt időt méri
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">ElapsedEventArgs e</param>
        private void Stopper_Elapsed(object sender, ElapsedEventArgs e)
        {
            TotalTime++;
            if (TotalTime % 5 == 0)
            {
                Speed += 3;
                this.UpdateTimer();
            }
        }

        /// <summary>
        /// időzítő tick eseménye
        /// </summary>
        /// <param name="sender">object sender</param>
        /// <param name="e">EventArgs e</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (!this.isStarted)
            {
                this.timer.Stop();
                this.StartGame();
            }

            if (this.isStarted)
            {
                this.snake.Move();
                this.InvalidateVisual();
                this.Collision();
            }
        }

        /// <summary>
        /// alapértelmezések beállítása, kígyó indulásának iránya, módosítók lerakása
        /// </summary>
        private void SetDefault()
        {
            Random random = new Random();
            Point[,] points = new Point[,]
            {
                { new Point(3, 3), new Point(WIDTH - 3, HEIGHT - 3) },
                { new Point(3, HEIGHT - 3), new Point(WIDTH - 3, 3) }
            };
            int rnd = random.Next(0, 2);

            this.snake = new Snake(points[rnd, 0], 2);

            this.powerUps = new PowerUp[LOOTS];
            for (int i = 0; i < this.powerUps.Length; i++)
            {
                this.powerUps[i] = new PowerUp(this.snake.Parts.ToList(), this.powerUps);
            }

            TotalTime = 0;
            Speed = 15;
            this.UpdateTimer();
        }

        /// <summary>
        /// timer frissítése, speed nem mehet minuszba és nem állhat meg a kígyó teljesen
        /// </summary>
        private void UpdateTimer()
        {
            if (Speed < 6)
            {
                Speed = 3;
            }

            if (this.timer != null)
            {
                this.timer.Interval = TimeSpan.FromMilliseconds(1000 / Speed);
            }
        }

        /// <summary>
        /// játék indítása, menü ablak hívása
        /// </summary>
        private void StartGame()
        {
            MenuWindow window = new MenuWindow();
            if (window.ShowDialog() == true)
            {
                this.isStarted = true;
                this.SetDefault();
                this.timer.Start();
                this.stopper.Start();
            }
            else
            {
                if (Application.Current != null)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        /// <summary>
        /// kígyó ütközése valamivel: módosítóval, magával, pálya széleivel
        /// </summary>
        private void Collision()
        {
            bool isDead = false;

            for (int i = 0; i < this.powerUps.Length; i++)
            {
                if (this.snake.Head.Equals(this.powerUps[i].Position))
                {
                    this.Loot(this.snake, i);
                }
            }

            for (int i = 1; i < this.snake.Parts.Count; i++)
            {
                if (this.snake.Head.Equals(this.snake.Parts[i]))
                {
                    isDead = true;
                }
            }

            if (this.snake.Head.X < 0 || this.snake.Head.X >= WIDTH ||
                this.snake.Head.Y < 0 || this.snake.Head.Y >= HEIGHT)
            {
                isDead = true;
            }

            if (isDead)
            {
                this.EndGame(isDead);
            }
        }

        /// <summary>
        /// kígyó felszedi a módosítót, akkor belassul, felgyorsul vagy meghal
        /// </summary>
        /// <param name="snake">Snake snake</param>
        /// <param name="i">int i</param>
        private void Loot(Snake snake, int i)
        {
            if (this.powerUps[i].CurrentColor.Color == Colors.Black)
            {
                this.EndGame(true);
            }
            else
            {
                if (this.powerUps[i].CurrentColor.Color == Colors.Red)
                {
                    Speed -= 3;
                    this.UpdateTimer();
                }
                else
                {
                    Speed += 3;
                    this.UpdateTimer();
                }

                snake.Loot();
                this.powerUps[i].Create(this.snake.Parts, this.powerUps);
            }
        }

        /// <summary>
        /// játék vége, deadwindow hívása
        /// </summary>
        /// /// <param name="isDead">bool isDead</param>
        private void EndGame(bool isDead)
        {
            this.timer.Stop();
            this.stopper.Stop();
            this.isStarted = false;
            if (isDead)
            {
                DeadWindow window = new DeadWindow(this.snake.TotalPoints, TotalTime);
                if (window.ShowDialog() == true)
                {
                }
            }

            this.StartGame();
        }
    }
}
