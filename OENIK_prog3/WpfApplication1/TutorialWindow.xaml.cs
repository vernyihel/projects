﻿// <copyright file="TutorialWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for TutorialWindow.xaml
    /// tutoriál ablaka
    /// </summary>
    public partial class TutorialWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TutorialWindow"/> class.
        /// default ctor
        /// </summary>
        public TutorialWindow()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
