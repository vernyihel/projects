﻿// <copyright file="PowerUp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// felvehető módosítók osztálya
    /// </summary>
    public class PowerUp
    {
        /// <summary>
        /// random szám generálás a módosítók típusaihoz
        /// </summary>
        private static Random random = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerUp"/> class.
        /// ctor: létrehozza a módosítókat
        /// </summary>
        /// <param name="parts">List parts</param>
        /// <param name="powerUps">PowerUp powerUps</param>
        public PowerUp(List<Point> parts, PowerUp[] powerUps)
        {
            this.Create(parts, powerUps);
        }

        /// <summary>
        /// Gets or sets
        /// módosító pozíció propertyje
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// Gets or sets
        /// módosító aktuális színe
        /// </summary>
        public SolidColorBrush CurrentColor { get; set; }

        /// <summary>
        /// módosítók létrehozása különböző színekben, átfedések vizsgálata h módosítót ne rakjon a kígyóra vagy másik módosítóra
        /// </summary>
        /// <param name="parts">List parts</param>
        /// <param name="powerUps">PowerUp powerUps</param>
        public void Create(List<Point> parts, PowerUp[] powerUps)
        {
            double x;
            double y;
            bool overlap;
            int rnd = random.Next(5);

            if (rnd == 0 || rnd == 1)
            {
                this.CurrentColor = new SolidColorBrush(Colors.Red);
            }
            else if (rnd == 2)
            {
                this.CurrentColor = new SolidColorBrush(Colors.Black);
            }
            else
            {
                this.CurrentColor = new SolidColorBrush(Colors.Blue);
            }

            do
            {
                overlap = false;
                x = random.Next(Game.WIDTH - 3);
                y = random.Next(Game.HEIGHT - 3);

                foreach (Point p in parts)
                {
                    if (p.X == x && p.Y == y)
                    {
                        overlap = true;
                        break;
                    }
                }

                for (int i = 0; i < powerUps.Length; i++)
                {
                    if (powerUps[i] != null && powerUps[i].Position.X == x && powerUps[i].Position.Y == y)
                    {
                        overlap = true;
                        break;
                    }
                }
            }
            while (overlap);

            this.Position = new Point(x, y);
        }
    }
}
