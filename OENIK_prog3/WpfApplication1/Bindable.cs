﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System.ComponentModel;

    /// <summary>
    /// alap ősosztály
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// propertychanged esemény
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// property változás
        /// </summary>
        /// <param name="name">string name</param>
        public void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
