﻿// <copyright file="DeadWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for DeadWindow.xaml
    /// kígyó halála ablak pontszám kiírással
    /// </summary>
    public partial class DeadWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeadWindow"/> class.
        /// default ctor
        /// </summary>
        public DeadWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeadWindow"/> class.
        /// ctornak pontok átadása
        /// </summary>
        /// <param name="points">int points</param>
        /// <param name="time">int time</param>
        public DeadWindow(int points, int time)
        {
            this.InitializeComponent();
            this.lbpoints.Content = points;
            this.lbtime.Content = time;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
