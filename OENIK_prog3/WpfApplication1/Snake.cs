﻿// <copyright file="Snake.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApplication1
{
    using System.Collections.Generic;
    using System.Windows;

    /// <summary>
    /// kígyó, játákos osztálya
    /// </summary>
    public class Snake
    {
        /// <summary>
        /// kígyó teste
        /// </summary>
        private List<Point> parts;

        /// <summary>
        /// következő irány
        /// </summary>
        private int nextDirection;

        /// <summary>
        /// aktuális irány
        /// </summary>
        private int currentDirection;

        /// <summary>
        /// elért pontok száma
        /// </summary>
        private int totalPoints;

        /// <summary>
        /// Initializes a new instance of the <see cref="Snake"/> class.
        /// ctor: kígyó elindulási helye és iránya
        /// </summary>
        /// <param name="start">Point start</param>
        /// <param name="startDirection">int startDirection</param>
        public Snake(Point start, int startDirection)
        {
            this.parts = new List<Point>();

            this.nextDirection = startDirection;
            this.currentDirection = startDirection;

            this.parts.Add(start);
            this.parts.Add(new Point((startDirection == 2) ? start.X - 1 : start.X + 1, start.Y));
            this.parts.Add(new Point((startDirection == 2) ? start.X - 2 : start.X + 2, start.Y));
        }

        /// <summary>
        /// Gets or sets
        /// elért összpontszám propja
        /// </summary>
        public int TotalPoints
        {
            get
            {
                return this.totalPoints;
            }

            set
            {
                this.totalPoints = value;
            }
        }

        /// <summary>
        /// Gets or sets
        /// </summary>
        public List<Point> Parts
        {
            get { return this.parts; }
            set { this.parts = value; }
        }

        /// <summary>
        /// Gets
        /// kígyó feje, azaz részeinek 1. eleme
        /// </summary>
        public Point Head
        {
            get
            {
                return this.parts[0];
            }
        }

        /// <summary>
        /// Gets or sets
        /// következő irány meghatározása és kígyó ne mozogjon saját magába
        /// </summary>
        public int NextDirection
        {
            get
            {
                return this.nextDirection;
            }

            set
            {
                if ((value == 1 && this.currentDirection == 3) || (value == 2 && this.currentDirection == 4) || (value == 3 && this.currentDirection == 1) || (value == 4 && this.currentDirection == 2))
                {
                    return;
                }

                this.nextDirection = value;
            }
        }

        /// <summary>
        /// módosítók felszedése és kígyó méretének, pontszámának növelése
        /// </summary>
        public void Loot()
        {
            Point newOne = this.parts[this.parts.Count - 1];
            this.parts.Add(newOne);
            this.totalPoints++;
        }

        /// <summary>
        /// kígyó mozgatása
        /// </summary>
        public void Move()
        {
            for (int i = this.parts.Count - 1; i > 0; i--)
            {
                this.parts[i] = this.parts[i - 1];
            }

            switch (this.NextDirection)
            {
                case 1:
                    this.parts[0] = new Point(this.parts[0].X, this.parts[0].Y - 1);
                    break;
                case 2:
                    this.parts[0] = new Point(this.parts[0].X + 1, this.parts[0].Y);
                    break;
                case 3:
                    this.parts[0] = new Point(this.parts[0].X, this.parts[0].Y + 1);
                    break;
                case 4:
                    this.parts[0] = new Point(this.parts[0].X - 1, this.parts[0].Y);
                    break;
            }

            this.currentDirection = this.NextDirection;
        }
    }
}
