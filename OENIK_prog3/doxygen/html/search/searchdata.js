var indexSectionsWithContent =
{
  0: "abcdghilmnoprstwx",
  1: "abdgmpst",
  2: "wx",
  3: "abdgmprstw",
  4: "acdgilmopst",
  5: "hlsw",
  6: "chnpst",
  7: "p",
  8: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties",
  7: "events",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties",
  7: "Events",
  8: "Pages"
};

